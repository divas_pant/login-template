# Login Template

Login template built using flask and flask login. Useful for prototyping.

## Requires
```
Python>=3.7
```

## Technical Stack
- Flask
- Sqlite


## Installation
Checkout latest version from git using:
```bash
git clone <Repository url> <optional path to folder>
```

### Using Pipenv
- Install pipenv if not already available using pip install command
```bash
pip install pipenv
```
or
```python
pip3 install pipenv
```
- cd to the root of the folder

```bash
cd /<Path to folder>/login-template
```

- To setup the environment using the locked dependencies
```python
pipenv install --deploy
```

- To set up the project with latest version of libraries wherever applicable use
``` python
pipenv install
```

## Usage
### Environment variables
The program requires following environment variables to run properly, they can be set using .env file or calling set/export for each variable. In case where system falls back to defaults, it is mentioned in the variable description.

 - **ENV**: environment in which the project is running, defaults to dev if not provided.
 - **PORT**: Port on which the server will be listening, defaults to 5000.
 - **DB_URI**: URI for the database server used by AqlAlchemy, defaults to _working-directory/user.db._
 - **SECRET_KEY**: Required for session management, defaults to random values, will not persist between restarts.
 - **SECURITY_PASSWORD_SALT**: Required for generating validation tokens, defaults to random values, will not persist between restarts.
 - **ADMIN_EMAIL**: email id for the admin account which is created during initialization.
 - **EMAIL_USERNAME**: Username/sender email from which emails will be sent.
 - **EMAIL_PASSWORD**: Password for the given username.
 - **MAIL_SERVER**: Email server used to send email, defaults to gmail.
 - **MAIL_PORT**: Port for configured email server, defaults to 465.

To run the project, activate the virtual environment by using
```python
pipenv shell
```
Run the project from the index file by calling
```python
python main.py
```

One the server initialization is complete, open your browser at localhost:8050 to test. The system is initialized with a default user with admin as username and password, and a dummy email id. To initialize admin with a proper email id, see environment variables.
