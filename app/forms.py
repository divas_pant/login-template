from flask_wtf import FlaskForm
from wtforms import (BooleanField, PasswordField,
    widgets, StringField, SelectMultipleField, SubmitField)
from wtforms.ext.sqlalchemy.fields import QuerySelectMultipleField, QuerySelectField
from wtforms.validators import DataRequired, Email, Length, EqualTo

from models.user import User


class LoginForm(FlaskForm):
    username = StringField('Username',
        validators=[DataRequired(message="Username cannot be empty")])
    password = PasswordField('Password',
        validators=[DataRequired(message="Password required")])
    submit = SubmitField('Login')
    remember_me = BooleanField('Remember Me')


class RegistrationForm(FlaskForm):
    username = StringField('Username',
        validators=[DataRequired(), Length(min=5, max=16, message=f"Username must be between 5 & 16 characters long")])
    email = StringField('Email',
        validators=[DataRequired(message="Email cannot be empty"), Email("Not a valid email")])
    password = PasswordField('Password',
        validators=[DataRequired("Password cannot be empty"), Length(min=4, max=16, message="Password must be 4 characters long"), EqualTo('confirm', message='Passwords must match')])
    confirm = PasswordField('Re-enter password')
    submit = SubmitField('Register')


class AddUserForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email("Not a valid email")])
    submit = SubmitField('Submit')


class PasswordResetForm(FlaskForm):
    password = PasswordField('Password',
        validators=[DataRequired("Password cannot be empty"), Length(min=4, max=16, message="Password must be 4 characters long"), EqualTo('confirm', message='Passwords must match')])
    confirm = PasswordField('Re-enter password')
    submit = SubmitField('Reset')


class AddClientForm(FlaskForm):
    name = StringField('Client Name', validators=[DataRequired("Field cannot be empty")])
    domain = StringField('Email@', validators=[DataRequired()])
    navigation = QuerySelectMultipleField(
            'Nav Links Enabled',
            query_factory=lambda: NavLink.query.all(),
            get_label=lambda nav: nav.link
            )
    submit = SubmitField('Add')


class AdminToolsForm(FlaskForm):
    client = QuerySelectField(
            'Select Client',
            query_factory=lambda: Client.query.all(),
            get_label=lambda client: client.name
            )
    # users = QuerySelectField(
    #         'Select Client',
    #         query_factory=lambda: User.query.filter_by(client_id=client).all(),
    #         get_label=lambda client: client.name
    #         )
    # navigation = QuerySelectMultipleField(
    #         'Nav Links Enabled',
    #         query_factory=lambda: NavLink.query.all(),
    #         get_label=lambda nav: nav.link
    #         )
    submit = SubmitField('Update')
