from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect
from flask_session import Session
from flask_wtf.csrf import CSRFProtect

'''
This module initializes various flask extensions used within code.
'''
db = SQLAlchemy()
login_manager = LoginManager()
session = Session()
csrf = CSRFProtect()
