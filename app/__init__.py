from flask import Flask, g
from flask_login import login_required, LoginManager

from config.config import Config as config


def create_app():
    '''Initializes Flask app using app_factory pattern, and registers
    db, routes and other components.
    flask_server will/can be registered by middleware during the server initilization.'''

    server = Flask(__name__, instance_relative_config=False)

    server.config.from_object(config)

    register_blueprints(server)

    register_resources(server)

    return server


def register_blueprints(app):
    '''Registers blueprints that manage incoming requests routes.'''
    from routes.auth import auth
    app.register_blueprint(auth)


def register_resources(app):
    '''Registers with the flask app, database, login manager, session, & emailer resources'''
    from app.extensions import db
    from app.extensions import login_manager
    from app.extensions import session
    from app.extensions import csrf
    from models.init_db_data import initialize_data

    db.init_app(app)

    csrf.init_app(app)

    login_manager.init_app(app)
    login_manager.login_view = 'web.login'

    app.config['SESSION_SQLALCHEMY'] = db
    app.config['SESSION_SQLALCHEMY_TABLE'] = 'sessions'
    session.init_app(app)


    with app.test_request_context():
        '''Initializes the db when the server is first run. In case of repeated runs, it checks for existence of
        db, in the location specified in config. If db exists the command does nothing.'''
        # db.drop_all()
        db.create_all()
        initialize_data(db)
