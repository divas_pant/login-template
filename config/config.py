import os
import logging
import string
import random

class Config(object):
    HOST = '0.0.0.0'
    PORT = os.environ.get('PORT', 5000)
    SQLALCHEMY_DATABASE_URI = os.environ.get('DB_URI', 'sqlite:///'+os.getcwd()+'/user.db')
    SECRET_KEY = os.environ.get("SECRET_KEY",
        ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(50)))
    SECURITY_PASSWORD_SALT = os.environ.get('SECURITY_PASSWORD_SALT',
        ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(30)))
    DEBUG = True
    ENV= os.environ.get('ENV', 'dev')
    SESSION_COOKIE_SECURE = False
    SESSION_TYPE = 'sqlalchemy'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    MAIL_SERVER = os.environ.get('EMAIL_SERVER', 'smtp.gmail.com')
    MAIL_PORT = os.environ.get('EMAIL_PORT', '465')
    MAIL_PASSWORD = os.environ.get('EMAIL_PASSWORD')
    MAIL_DEFAULT_SENDER = os.environ.get('EMAIL_USERNAME')


logger = logging.getLogger('User_login')
