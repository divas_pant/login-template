from flask_sqlalchemy import SQLAlchemy
from datetime import datetime, timedelta
from flask_login import UserMixin
from werkzeug.security import check_password_hash
from werkzeug.security import generate_password_hash

from app.extensions import db, login_manager


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


class User(UserMixin, db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(30), nullable=False, unique=True, index=True)
    email = db.Column(db.String(60), nullable=False, index=True, unique=True)
    password = db.Column(db.String(256), nullable=False)
    last_login = db.Column(db.DateTime(), default=None)
    verified = db.Column(db.Boolean, default=False)
    verified_on = db.Column(db.DateTime(), default=None)
    authenticated = db.Column(db.Boolean, default=False)
    registered_on = db.Column(db.DateTime(), default = datetime.now)
    access_scope = db.Column(db.String(50), default='all')
    is_super = db.Column(db.Boolean, default=False)
    req_passwd_reset = db.Column(db.Boolean, default=False)

    def is_active(self):
        return self.verified

    def get_id(self):
        return self.id

    def is_authenticated(self):
        return self.authenticated

    def is_anonymous(self):
        return False

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def set_client_id(self, id):
        self.client_id = id

    def update_last_login(self):
        self.last_login = datetime.now()

    def activate_user(self):
        self.verified = True
        self.verified_on = datetime.now()

    def disable_user(self):
        self.verified = False

    def verify_password(self, password):
        return check_password_hash(self.password, password)

    def is_admin(self):
        return self.is_super

    def pwd_reset_on(self):
        self.req_passwd_reset = True

    def pwd_reset_off(self):
        self.req_passwd_reset = False

    def __repr__(self):
        return f'User {self.email}'
