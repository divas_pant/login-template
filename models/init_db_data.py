import os
from datetime import datetime
from sqlalchemy import exc
from models.user import User


def _insert_admin(db):
    user = User(email=os.environ.get("ADMIN_EMAIL", "dummy@email.com"),
        username='admin',
        verified = True,
        verified_on = datetime.now(),
        is_super = True
        )
    user.set_password('admin')
    db.session.add(user)
    db.session.commit()

def initialize_data(db):
    try:
        _insert_admin(db)
    except Exception as e:
        pass
