import os
import smtplib
import time
from email.mime.base import MIMEBase
from email import encoders
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from itsdangerous import URLSafeTimedSerializer

from app import config as config
from config.config import logger


class TokenHelper:
    ''' Contains static methods that are used for generating and decoding tokens sent to the user
    for verification purposes.'''
    @staticmethod
    def generate_confirmation_token(email):
        serializer = URLSafeTimedSerializer(config.SECRET_KEY)
        return serializer.dumps(email, salt=config.SECURITY_PASSWORD_SALT)

    @staticmethod
    def confirm_token(token, expiration=3600):
        serializer = URLSafeTimedSerializer(config.SECRET_KEY)
        try:
            email = serializer.loads(
                token,
                salt=config.SECURITY_PASSWORD_SALT,
                max_age=expiration
            )
        except:
            return False
        return email


class EmailHelper:
    def __init__(self):
        try:
            server = smtplib.SMTP_SSL(config.MAIL_SERVER,config.MAIL_PORT)
        except:
            print("Error")
            logger.warning(f'Failed connecting to server {config.MAIL_SERVER}, retrying in 5 secs...')
            time.sleep(5)
            server = smtplib.SMTP_SSL(config.MAIL_SERVER, config.MAIL_PORT)

        server.ehlo()
        server.login(config.MAIL_DEFAULT_SENDER,config.MAIL_PASSWORD)
        self.server = server


    def __del__(self):
        self.server.quit()


    def send_mail(self, subject, body, destination,attachment=[], html=True,cc=None, bcc=None):
        '''
        send mail message
        '''
        # Create message container - the correct MIME type is multipart/mixed here!
        if html:
            MESSAGE = MIMEMultipart('mixed')
        else:
            MESSAGE = MIMEMultipart()
        MESSAGE['From'] = config.MAIL_DEFAULT_SENDER
        MESSAGE['To'] = destination
        MESSAGE['Subject'] = subject

        if cc:
            MESSAGE['Cc'] = cc
            recip_list=destination+","+cc
        else:
            recip_list=destination

        #Adding BCC Field
        if bcc:
            if isinstance(bcc,str):
                recip_list = recip_list + "," + bcc
            elif isinstance(bcc, list):
                recip_list = recip_list + "," + ",".join(bcc)


        if html:
            MESSAGE.preamble = """"""
            MESSAGE.attach(MIMEText(body, 'html'))
        else:
            MESSAGE.attach(MIMEText(body))

        if attachment:
            if not isinstance(attachment,(list,tuple)): attachment=[attachment]
            for att in attachment:
                part = MIMEBase('application', "octet-stream")
                att_file = open(att,"rb")
                part.set_payload(att_file.read())
                att_file.close()
                encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(att))
                MESSAGE.attach(part)

        try:
            self.server.sendmail(config.MAIL_DEFAULT_SENDER, recip_list.split(','), MESSAGE.as_string())
            logger.info(f"Sent email to {recip_list}")
        except Exception as e:
            logger.error(f"Could not sent the email due to some error: {e}")
