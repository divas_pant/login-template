import logging.config
from app import create_app
from config.config import logger
from werkzeug.middleware.dispatcher import DispatcherMiddleware
from werkzeug.serving import run_simple

server = create_app()


if __name__=='__main__':
    logging.config.fileConfig('config/logger.conf', disable_existing_loggers=True)
    logger.propagate = False
    logger.info('Starting User Login Module')
    logger.debug(f"server starting at {server.config['HOST']}:{server.config['PORT']}")

    app = DispatcherMiddleware(server)
    run_simple(server.config['HOST'], server.config['PORT'], app, use_reloader=True)
