import base64
from flask import (Blueprint, request, render_template, redirect,
    url_for, session, g, flash, abort)
from flask_login import current_user, login_required, login_user, logout_user
from werkzeug.urls import url_parse
from sqlalchemy import exc

from app.forms import LoginForm
from app.forms import RegistrationForm
from app.forms import AddUserForm
from app.forms import PasswordResetForm
from app.forms import AddClientForm
from app.forms import AdminToolsForm
from app.extensions import db
from config.config import logger
from helpers.helpers import TokenHelper as tokens, EmailHelper as Emailer
from models.user import User

auth = Blueprint('web', __name__)


@auth.route('/')
def index():
    '''Default route.
    Users can be redirected to this function when they need to be taken to
    the home page.
    '''
    if current_user.is_authenticated:
        return redirect('/home')
    return redirect(url_for('web.login'))

@auth.route('/login', methods=['GET', 'POST'])
def login():
    '''Handles the user login. If method type is GET it will check for user being logged in
    if so, it redirects user to home, if not, it shows userLogged in.
    '''
    # checks if the user is authenticated, if so redirects them to home
    if current_user.is_authenticated:
        logger.info(f'User {current_user.username} is already logged in ')
        session.pop('user', None)
        session['user'] = current_user.username
        return redirect('/home')
    form = LoginForm(request.form)
    if form.validate_on_submit():
        # get the user from db by their username
        user = User.query.filter_by(username=form.username.data).first()

        # check if user exists and if so the password matches, if not return generic error
        if user is None or not user.verify_password(form.password.data):
            error = 'Invalid username or password'
            logger.warning(f"Error in user login data")
            return render_template('login.html', form=form, error=error)

        # check if the user is verified, if not will ask user to check email
        if not user.verified:
            error = 'Your account is not active, please check your email for instructions.'
            logger.warn(f"Unverified user {user.email} tried to login without success")
            return render_template('login.html', form=form, error=error)

        # if checks pass, login user
        login_user(user, remember=form.remember_me.data)
        user.authenticated = True
        user.update_last_login()
        try:
            db.session.commit()
        except (exc.IntegrityError, Exception) as e:
            db.session.rollback()
            logger.error(f"Some error occured in logging in the user. Error: {e}")
            flash("Some error occured, plase try again in some time.")
            return redirect(url_for('web.login'))

        logger.info(f"User {user.email} logged in.")
        session.pop('user', None)
        session['user'] = form.username.data

        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = '/home'
        return redirect(next_page)

    return render_template('login.html', title='Sign In', form=form)


@auth.route('/logout')
@login_required
def logout():
    ''' Handles user log out '''
    logger.info(f"Logging out User {session['user']}")
    user = current_user
    user.authenticated = False
    logout_user()
    try:
        db.session.commit()
    except exc.IntegrityError:
        db.session.rollback()
        logger.error(f'Error updating user logout instance in db for {session["user"]}')
    session.pop('user')
    logger.debug("User logged out successfully")
    return redirect(url_for('web.login'))


@auth.route('/register', methods=['GET', 'POST'])
def register():
    '''Renders the user registration form and allows for a new user to complete
    their registation for the access.
    '''
    # Check if user is accessing link in existing session, abort  if true
    if current_user.is_authenticated:
        return redirect(url_for('web.index'))
    form = RegistrationForm(request.form)
    error = None
    if request.method == 'POST':
        logger.debug("recieved new user registration request")
        if form.validate_on_submit():
            user = User(username=form.username.data,
                        email=form.email.data)
            user.set_password(form.password.data)
            db.session.add(user)
            try:
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                error = f"Some Error Occured, please try again"
                logger.warning(f'Could not commmit changes to the database: {e}')
            except exc.IntegrityError:
                db.session.rollback()
                error = f'Username already taken, try again with a different username'
                logger.info(f"User {user.email} tried registering with existing username: '{user.username}'")
            if not error:
                logger.info(f"Added new user {user.email} to users table")
                token = tokens.generate_confirmation_token(user.email)
                confirm_url = url_for('web.confirm_email', token=token, _external=True)
                message = render_template('confirmation_email.html', confirm_url=confirm_url)
                Emailer().send_mail("Confirm Account", message, user.email)

                flash("Registration successful, please check your email to continue with account registration.")
                return redirect(url_for('web.login'))
            else:
                return render_template('register.html', title="Register User", form=form, error=error)
        error = "Error Registering User, Try again"
        return render_template('register.html', title="Register User", form=form, error=error)
    return render_template('register.html', title="Register User", form=form)


@auth.route('/confirm_email/<token>', methods=['GET'])
def confirm_email(token):
    '''Validates the email token provided by the user. On validation redirects user
        to the edit profile page to complete the process.
    '''
    if current_user.is_authenticated:
        ''' If current user is authenticated, they cannot validate another token.'''
        logger.warning(f"Attempt from {current_user.username} to access external token via email link in existing session")
        abort(404)
    try:
        email = tokens.confirm_token(token, expiration=3600)

    except Exception as e:
        logger.warning("User Validation token is invalid or expired")
        flash("Invalid Token")
        abort(404)

    # double check if the email belongs to valid user
    user = User.query.filter_by(email=email).first_or_404()

    if not user:
        # log the error as it is a special case, and redirect user to login
        logger.warning(f'An attempt to verify a token was made for user {email}, but user not found.')
        flash("Error in verification.")
        return redirect(url_for('web.login'))
    else:
        # all checks pass, activate user and redirect them to login
        logger.info(f"User email token confirmed for {user.email}, redirecting to registration")
        user.activate_user()
        db.session.commit()
        return redirect(url_for('web.login'))



@auth.route('/request-password-reset', methods=['GET', 'POST'])
def request_password_reset():
    ''' Initializes a password reset request for a user. THe user is ientified by their email id.
    If email exists in db the user gets a password reset link. If not nothing happens, and the return message
    is same in both cases.
    To avoid using same link multiple times, an additional parameter "req_passwd_reset"
    is set to true until the user resets their password.
    The only time a different message is shown to the end user is when they are registered for an account but
    have not yet activated their account from the link. '''

    form = AddUserForm(request.form)
    if request.method == "POST":
        if form.validate_on_submit():
            email = form.email.data
            user = User.query.filter_by(email=email).first()
            if user:
                if not user.verified:
                    logger.warning(f'User {user.email} requested password reset for an unverified account')
                    flash("Your account is not verified, \n follow the instructions on your email to continue.")
                    return redirect(url_for('web.login'))
                # generate verification token and the email teplate
                token = tokens.generate_confirmation_token(user.email)
                confirm_url = url_for('web.reset_password_by_email', token=token, _external=True)
                html = render_template('password_reset.html', confirm_url=confirm_url)
                subject = "Reset Password"
                Emailer().send_mail(subject, html, user.email)

                # Update parameters on the db to mark user as one with password reset request active
                user.pwd_reset_on()
                db.session.commit()
                logger.info(f"User account {user.email} password reest request is enabled.")
            else:
                logger.warning(f"A password reset request was made for an unregistered email: {email}")
            flash("Please follow the instructions on your e-mail to continue with the password reset")
            return redirect(url_for('web.login'))
    return render_template('req_password_reset.html', form=form)


@auth.route('/reset-password/<token>', methods=["GET", "POST"])
def reset_password_by_email(token):
    '''Loads the reset password form where a valid user can update their password in case it is lost.
    The token parameter verifies that the user is a valid user, after the token verification, req_passwd_reset
    parameter is checked for the user. If it is false, it implies either the user already used the link
    to reset their password, or somehow the user manage to forge the token. In either case, a token expired message
    is sent to the user.'''

    try:
        email = tokens.confirm_token(token, expiration=3600)
    except Exception as e:
        logger.warning(f'Reset password was accessed with an invlid token')
        flash("Invalid Token")
        return redirect(url_for('web.login'))
    user = User.query.filter_by(email=email).first()

    ''' If user is not found ot user is not verified, return 404'''
    if not user or not user.verified:
        logger.warning(f" User requesting password reset does not exists or is not verified")
        abort(404)

    '''If user tries to use the same link multiple times after using it once to reset password,
    redirect to login page saying link has expired.'''
    if not user.req_passwd_reset:
        logger.warning(f"User {user.email} accessed password reset with an expired link")
        flash("Link Expired, please request a \n new password reset link to continue")
        return redirect(url_for('web.login'))

    form = PasswordResetForm(request.form)
    if request.method == "POST":
        logger.debug("Initializing password reset request for user")
        if form.validate_on_submit():
            user.set_password(form.password.data)
            user.pwd_reset_off()
            try:
                db.session.commit()
                logger.info(f"User {user.email} password was reset successfully.")
            except Exception as e:
                logger.error(f"Password reset could not be done for user {user.email}. Error: {e} ")
                return redirect(url_for('web.reset_password_by_email'))
            flash("Password updated successfully, \n login to continue")
            return redirect(url_for('web.login', token=token))
    return render_template('reset_password.html', form=form)
